package il.ac.technion.cs.ai;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.core.Attribute;
import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.StringToNominal;

import com.echonest.api.v4.EchoNestException;
import com.echonest.api.v4.Song;

/**
 * A little demo java program for using WEKA.<br/>
 * Check out the Evaluation class for more details.
 * 
 * @author FracPete (fracpete at waikato dot ac dot nz)
 * @see Evaluation
 */

public class Weka {
	/** the training file */
	protected String m_TrainingFile = null;

	/** the training instances */
	protected Instances m_Training = null;

	/**
	 * initializes the demo
	 * 
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public Weka(String training_file) throws FileNotFoundException, IOException {
		m_TrainingFile = training_file;
		m_Training = new Instances(new BufferedReader(new FileReader(
				m_TrainingFile)));
		m_Training.setClassIndex(m_Training.numAttributes() - 1);
	}

	/**
	 * performs 10-fold cross validation for solo learning
	 * 
	 * @param classifierString
	 * @return evaluation result
	 * @throws Exception
	 */
	private Evaluation solo_eval(String classifierString) throws Exception {
		StringToNominal filter = new StringToNominal();
		filter.setInputFormat(m_Training);

		String[] options;
		if (m_TrainingFile.contains("solograde"))
			options = weka.core.Utils.splitOptions("-R 1");
		else
			options = weka.core.Utils.splitOptions("-R 1,8");

		filter.setOptions(options);
		Instances filtered = Filter.useFilter(m_Training, filter);

		// 10-fold CV with seed=1
		Evaluation m_Evaluation = new Evaluation(filtered);

		String[] class_options;
		if (classifierString.contains("J48"))
			class_options = null;
		else
			class_options = weka.core.Utils.splitOptions("-K 5");

		m_Evaluation.crossValidateModel(classifierString, filtered, 10,
				class_options, m_Training.getRandomNumberGenerator(1));

		return m_Evaluation;
	}

	/**
	 * performs 10-fold cross validation for community learning
	 * 
	 * @param classifierString
	 * @return evaluation result
	 * @throws Exception
	 */
	private Evaluation comm_eval(String classifierString) throws Exception {
		StringToNominal filter = new StringToNominal();
		filter.setInputFormat(m_Training);

		String[] options;
		if (m_TrainingFile.contains("commgrade"))
			options = weka.core.Utils.splitOptions("-R 2");
		else
			options = weka.core.Utils.splitOptions("-R 2,9");

		filter.setOptions(options);
		Instances filtered = Filter.useFilter(m_Training, filter);

		Instances comm_only = new Instances(filtered);
		comm_only.delete();
		Instances user_only = new Instances(filtered);
		user_only.delete();
		for (int i = 0; i < filtered.numInstances(); ++i) {
			Instance ins = filtered.instance(i);

			if (ins.value(0) == 0)
				user_only.add(ins);
			else
				comm_only.add(ins);
		}

		Random rand = new Random(1);
		user_only.randomize(rand);

		String[] class_options;
		if (classifierString.contains("J48"))
			class_options = null;
		else
			class_options = weka.core.Utils.splitOptions("-K 5");

		// 10-fold CV with seed=1
		int folds = 10;
		Evaluation m_Evaluation = new Evaluation(filtered);
		for (int n = 0; n < folds; n++) {
			Instances train = new Instances(comm_only);
			Instances train_add = user_only.trainCV(folds, n);
			for (int i = 0; i < train_add.numInstances(); ++i) {
				train.add(train_add.instance(i));
			}
			Instances test = user_only.testCV(folds, n);
			// the above code is used by the StratifiedRemoveFolds filter, the
			// code below by the Explorer/Experimenter:
			// Instances train = randData.trainCV(folds, n, rand);

			// build and evaluate classifier
			Classifier cls = Classifier
					.forName(classifierString, class_options);
			cls.buildClassifier(train);
			m_Evaluation.evaluateModel(cls, test);
		}

		return m_Evaluation;
	}

	/**
	 * runs 10-fold CV over the training file
	 */
	public Evaluation evaluate(String classifierName) throws Exception {
		if (m_TrainingFile.contains("solo"))
			return solo_eval(classifierName);

		return comm_eval(classifierName);
	}

	/**
	 * @return vector of solo grade attributes
	 */
	public static FastVector userSongGradeAttr() {
		// Declare string attributes
		Attribute artist = new Attribute("artist", (FastVector) null);

		// Declare numeric attributes along with values
		Attribute dancebility = new Attribute("dancebility");
		Attribute energy = new Attribute("energy");
		Attribute loudness = new Attribute("loudness");
		Attribute songHotness = new Attribute("songHotness");
		Attribute artistHotness = new Attribute("artistHotness");
		Attribute artistFamiliarity = new Attribute("artistFamiliarity");

		// Declare the class attribute along with its values
		FastVector fvClassVal = new FastVector(3);
		fvClassVal.addElement("Once");
		fvClassVal.addElement("Repeat");
		fvClassVal.addElement("Loved");
		Attribute ClassAttribute = new Attribute("preference", fvClassVal);

		// Declare the feature vector
		FastVector fvWekaAttributes = new FastVector(10);

		// fvWekaAttributes.addElement(title);
		fvWekaAttributes.addElement(artist);
		// fvWekaAttributes.addElement(key);
		// fvWekaAttributes.addElement(mode);
		fvWekaAttributes.addElement(dancebility);
		fvWekaAttributes.addElement(energy);
		fvWekaAttributes.addElement(loudness);
		fvWekaAttributes.addElement(songHotness);
		fvWekaAttributes.addElement(artistHotness);
		fvWekaAttributes.addElement(artistFamiliarity);
		fvWekaAttributes.addElement(ClassAttribute);

		return fvWekaAttributes;
	}

	/**
	 * @param song
	 * @param playCount
	 * @param attr
	 * @return grade instance
	 * @throws EchoNestException
	 */
	public static Instance userSongGradeInstance(Song song, int playCount,
			FastVector attr) throws EchoNestException {
		// Create the instance
		Instance instance = new Instance(8);

		instance.setValue((Attribute) attr.elementAt(0), song.getArtistName());
		instance.setValue((Attribute) attr.elementAt(1), song.getDanceability());
		instance.setValue((Attribute) attr.elementAt(2), song.getEnergy());
		instance.setValue((Attribute) attr.elementAt(3), song.getLoudness());
		instance.setValue((Attribute) attr.elementAt(4),
				song.getSongHotttnesss());
		instance.setValue((Attribute) attr.elementAt(5),
				song.getArtistHotttnesss());
		instance.setValue((Attribute) attr.elementAt(6),
				song.getArtistFamiliarity());

		// classify the instance
		if (playCount == 1)
			instance.setValue((Attribute) attr.elementAt(7), "Once");
		else if (playCount < 10)
			instance.setValue((Attribute) attr.elementAt(7), "Repeat");
		else
			instance.setValue((Attribute) attr.elementAt(7), "Loved");

		return instance;
	}

	/**
	 * @return vector for solo compare attributes
	 */
	public static FastVector userSongCompareAttr() {
		FastVector song = userSongGradeAttr();

		song.removeElementAt(song.size() - 1);

		FastVector fvWekaAttributes = new FastVector(2 * song.size() + 1);

		for (Object attr_obj : song.toArray()) {
			Attribute attr = (Attribute) attr_obj;
			Attribute new_attr = attr.copy(attr.name() + "1");
			fvWekaAttributes.addElement(new_attr);
		}

		for (Object attr_obj : song.toArray()) {
			Attribute attr = (Attribute) attr_obj;
			Attribute new_attr = attr.copy(attr.name() + "2");
			fvWekaAttributes.addElement(new_attr);
		}

		// Declare the class attribute along with its values
		FastVector fvClassVal = new FastVector(3);
		fvClassVal.addElement(">");
		fvClassVal.addElement("=");
		fvClassVal.addElement("<");
		Attribute ClassAttribute = new Attribute("relation", fvClassVal);

		fvWekaAttributes.addElement(ClassAttribute);

		return fvWekaAttributes;
	}

	/**
	 * @param song1
	 * @param song2
	 * @param attr
	 * @return compare instance
	 * @throws EchoNestException
	 */
	public static Instance userSongCompareInstance(Instance song1,
			Instance song2, FastVector attr) throws EchoNestException {

		Instance new_ins = new Instance(attr.size());

		for (int i = 0; i < song1.numAttributes() - 1; ++i) {
			Attribute a = (Attribute) attr.elementAt(i);
			Attribute b = (Attribute) attr.elementAt(i + song1.numAttributes()
					- 1);

			if (song1.attribute(i).isString() || song1.attribute(i).isNominal()) {
				new_ins.setValue(a, song1.stringValue(i));
				new_ins.setValue(b, song2.stringValue(i));
			} else {
				new_ins.setValue(a, song1.value(i));
				new_ins.setValue(b, song2.value(i));
			}
		}

		if (song1.classValue() > song2.classValue())
			new_ins.setValue((Attribute) attr.lastElement(), ">");
		else if (song1.classValue() < song2.classValue())
			new_ins.setValue((Attribute) attr.lastElement(), "<");
		else
			new_ins.setValue((Attribute) attr.lastElement(), "=");
		return new_ins;
	}

	/**
	 * saves dataset to ARFF file (in path)
	 * 
	 * @param dataset
	 * @param path
	 * @throws Exception
	 */
	public static void saveARFF(Instances dataset, File path) throws Exception {
		ArffSaver saver = new ArffSaver();

		// write grade file
		saver.setInstances(dataset);
		saver.setFile(path);
		saver.writeBatch();
	}

	/**
	 * @param attr
	 * @param community
	 * @param distances
	 * @return community dataset
	 */
	public static Instances communityDataset(FastVector attr,
			Map<String, Instances> community, Map<String, Double> distances) {

		Attribute distance = new Attribute("distance");
		attr.insertElementAt((Attribute) distance, 0);

		Instances dataset = new Instances("UserCommSongs", attr, 0);
		dataset.setClassIndex(attr.size() - 1);

		for (Entry<String, Instances> e : community.entrySet()) {
			for (int i = 0; i < e.getValue().numInstances(); ++i) {
				double dis = distances.get(e.getKey()).doubleValue();

				Instance ins = new Instance(attr.size());

				ins.setValue((Attribute) attr.firstElement(), dis);

				Instance original = e.getValue().instance(i);
				for (int j = 1; j < attr.size(); ++j) {
					if (original.attribute(j - 1).isString()
							|| original.attribute(j - 1).isNominal())
						ins.setValue((Attribute) attr.elementAt(j),
								original.stringValue(j - 1));
					else
						ins.setValue((Attribute) attr.elementAt(j),
								original.value(j - 1));
				}

				dataset.add(ins);
			}
		}

		return dataset;
	}
}
