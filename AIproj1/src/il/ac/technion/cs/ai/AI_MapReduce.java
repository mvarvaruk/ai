package il.ac.technion.cs.ai;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;

import weka.core.FastVector;
import weka.core.Instance;
import weka.core.Instances;

import com.echonest.api.v4.Song;

/**
 * @author Michael
 * 
 *         Static class for breaking the triplet file to user files in arff
 *         format. using Hadoop Map-Reduce.
 */
public class AI_MapReduce {

	/**
	 * @author Michael
	 * 
	 *         Map step - does nothing but declare user ID as reduce key
	 */
	private static class BlankMapper extends Mapper<Object, Text, Text, Text> {

		@Override
		protected void map(Object key, Text value, Context context)
				throws IOException, InterruptedException {

			String data = value.toString();

			String[] fields = data.split("	");

			Text new_key = new Text(fields[0]);
			Text new_value = new Text(fields[1] + "	" + fields[2]);

			context.write(new_key, new_value);
		}
	}

	/**
	 * @author Michael
	 * 
	 *         Reduce step - for each user creates a user file with song
	 *         features and preference class (both grade and compare)
	 * 
	 */
	private static class UserReducer extends Reducer<Text, Text, Text, Text> {

		protected void reduce(Text arg0, Iterable<Text> arg1, Context arg2)
				throws IOException, InterruptedException {

			String uid = arg0.toString();

			File userGradeFile = new File("new_sample/solograde_" + uid
					+ ".arff");
			File userCompareFile = new File("new_sample/solocompare_" + uid
					+ ".arff");

			if (!userGradeFile.exists()) {
				int song_count = 0;
				// int listen_count = 0;

				try {
					Map<String, Integer> plays = new HashMap<String, Integer>();

					for (Text par : arg1) {
						String[] params = par.toString().split("	");

						int num_of_plays = Integer.valueOf(params[1]);

						plays.put(params[0], num_of_plays);
					}

					Set<String> ids = plays.keySet();
					List<Song> songs = SongAPI.getSongsMetadata(ids);

					FastVector gradeAttr = Weka.userSongGradeAttr();
					Instances dataset_grade = new Instances("SoloGradeSongs",
							gradeAttr, 0);
					dataset_grade.setClassIndex(gradeAttr.size() - 1);

					FastVector compareAttr = Weka.userSongCompareAttr();
					Instances dataset_comapre = new Instances(
							"SoloComapreSongs", compareAttr, 0);
					dataset_comapre.setClassIndex(compareAttr.size() - 1);

					String songs_ids = "";

					for (Song s : songs) {
						Integer num_of_plays = plays.get(s.getID());
						if (num_of_plays == null)
							continue;

						songs_ids += "	" + s.getID();

						Instance ins = Weka.userSongGradeInstance(s,
								num_of_plays.intValue(), gradeAttr);
						dataset_grade.add(ins);

						song_count++;
						// listen_count += num_of_plays;
					}

					for (int i = 0; i < dataset_grade.numInstances(); ++i) {
						Instance song1 = dataset_grade.instance(i);

						for (int j = i + 1; j < dataset_grade.numInstances(); j++) {
							Instance song2 = dataset_grade.instance(j);

							Instance comp_ins = Weka.userSongCompareInstance(
									song1, song2, compareAttr);
							dataset_comapre.add(comp_ins);
						}
					}

					if (song_count >= 50) {
						Weka.saveARFF(dataset_grade, userGradeFile);
						Weka.saveARFF(dataset_comapre, userCompareFile);

						arg2.write(null, new Text(uid + songs_ids));
					}

					/*
					 * System.out.println("user " + uid + " reduced with " +
					 * listen_count + " plays for " + song_count + " songs");
					 */
				} catch (Exception ex) {
					ex.printStackTrace();
				}
			}
		}
	}

	public static final String output_dir_path = "new_sample/MRout";
	public static final String output_file_path = "new_sample/userSongSummery";

	/**
	 * @param DatasetReader
	 *            r - triplet file to be broken
	 * 
	 *            starts the map-reduce process.
	 */
	public static void dataset2userFiles(DatasetReader r) {
		Configuration conf = new Configuration();

		try {
			// first map reduce step
			Job job1;

			job1 = new Job(conf, "get metadata for songs");

			job1.setMapperClass(BlankMapper.class);
			job1.setReducerClass(UserReducer.class);

			job1.setMapOutputValueClass(Text.class);
			job1.setMapOutputKeyClass(Text.class);
			job1.setOutputKeyClass(Text.class);
			job1.setOutputValueClass(Text.class);

			job1.setInputFormatClass(TextInputFormat.class);
			job1.setOutputFormatClass(TextOutputFormat.class);

			FileInputFormat.addInputPath(job1, new Path(r.getFilePath()));
			FileOutputFormat.setOutputPath(job1, new Path(output_dir_path));

			job1.waitForCompletion(true);

			// merge output files
			File outputDir = new File(output_dir_path);
			File outputFile = new File(output_file_path);

			mergeFiles(outputDir.listFiles(), outputFile);

			// clean map-reduce HDFS
			delete(outputDir);

		} catch (IOException | ClassNotFoundException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * merging list of files to single txt file
	 * 
	 * @param files
	 * @param mergedFile
	 */
	public static void mergeFiles(File[] files, File mergedFile) {

		FileWriter fstream = null;
		BufferedWriter out = null;
		try {
			fstream = new FileWriter(mergedFile, true);
			out = new BufferedWriter(fstream);
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		for (File f : files) {
			if (f.getPath().contains(".crc"))
				continue;

			System.out.println("merging: " + f.getName());
			FileInputStream fis;
			try {
				fis = new FileInputStream(f);
				BufferedReader in = new BufferedReader(new InputStreamReader(
						fis));

				String aLine;
				while ((aLine = in.readLine()) != null) {
					out.write(aLine);
					out.newLine();
				}

				in.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * deletes all file or subfiles in folder
	 * 
	 * @param f
	 * @throws IOException
	 */
	public static void delete(File f) throws IOException {
		if (f.isDirectory()) {
			for (File c : f.listFiles())
				delete(c);
		}
		if (!f.delete())
			throw new FileNotFoundException("Failed to delete file: " + f);
	}
}
