package il.ac.technion.cs.ai;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import com.echonest.api.v4.EchoNestAPI;
import com.echonest.api.v4.EchoNestException;
import com.echonest.api.v4.Song;
import com.echonest.api.v4.SongParams;

/**
 * @author Michael
 * 
 *         all methods to work with EchoNest API
 */
public class SongAPI {
	final private static String api_key = "69PC1FLHB1GCRE6EG";

	/**
	 * @param ids
	 * @return a list of songs corresponding to the list of ids
	 */
	private static List<Song> sendRequest(List<String> ids) {
		EchoNestAPI api = new EchoNestAPI(api_key);

		// api.setTraceSends(true);
		api.setTraceRecvs(false);

		SongParams p = new SongParams();
		p.includeArtistHotttnesss();
		p.includeSongHotttnesss();
		p.includeArtistFamiliarity();
		p.includeAudioSummary();

		for (String id : ids) {
			p.add("id", id);
		}

		List<Song> lst = null;

		boolean flag = true;
		while (flag) {
			try {
				lst = api.getSongs(p);
				flag = false;
			} catch (EchoNestException ex) {
				flag = true;
			}
		}

		return lst;
	}

	/**
	 * breaks set to subsets of 10. sending requests and combining results.
	 * 
	 * @param ids
	 * @return a list of songs corresponding to the list of ids
	 */
	public static List<Song> getSongsMetadata(Set<String> ids) {
		final int max_ids = 10;

		List<Song> result = new ArrayList<Song>();

		List<String> ids_lst = new ArrayList<String>(ids);

		int buckets = ids_lst.size() / max_ids;
		buckets += (ids_lst.size() % max_ids == 0) ? 0 : 1;

		for (int i = 0; i < buckets; i++) {
			int start = i * max_ids;
			int end = (i + 1) * max_ids;
			end = (end < ids_lst.size()) ? end : ids_lst.size();

			List<String> sublist = ids_lst.subList(start, end);

			result.addAll(sendRequest(sublist));
		}

		return result;
	}
}
