package il.ac.technion.cs.ai;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;

import weka.classifiers.Evaluation;
import weka.core.Instances;

public class MainClass {

	public static final String original_dataset = "train_triplets.txt";

	/**
	 * @param f
	 *            - target file name
	 * @return map user to song list
	 * @throws Exception
	 */
	private static Map<String, List<String>> getUserSongList(File f)
			throws Exception {
		HashMap<String, List<String>> map = new HashMap<String, List<String>>();

		BufferedReader br = new BufferedReader(new FileReader(f));
		try {
			String line;
			line = br.readLine();
			while (line != null) {
				String[] params = line.split("	");

				List<String> list = new ArrayList<String>(Arrays.asList(params));
				list.remove(0);

				map.put(params[0], list);

				line = br.readLine();
			}
		} catch (Exception e) {
			throw e;
		} finally {
			br.close();
		}

		return map;
	}

	/**
	 * @param lst1
	 * @param lst2
	 * @return Jaccard distance between to song sets (lists)
	 */
	private static double calculateDistance(List<String> lst1, List<String> lst2) {
		/*
		 * using Jaccard distance = 1 - (A ^ B)/(A & B)
		 */

		int union = lst1.size() + lst2.size();

		if (union == 0)
			return 0;

		int inter = 0;
		for (String s : lst1) {
			if (lst2.contains(s)) {
				inter++;
			}
		}

		union -= inter;

		return 1 - ((double) inter / union);
	}

	/**
	 * @param uid
	 * @param map
	 * @return map of distances between all users to base user (uid)
	 */
	private static Map<String, Double> getDistanceMap(String uid,
			Map<String, List<String>> map) {
		HashMap<String, Double> distances = new HashMap<String, Double>();

		for (Entry<String, List<String>> e : map.entrySet()) {
			distances.put(e.getKey(),
					calculateDistance(e.getValue(), map.get(uid)));
		}

		return distances;
	}

	/**
	 * @param dir
	 * @param filter
	 * @return all users datasets of one kind (determined by filter)
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	private static Map<String, Instances> getDatasets(File dir,
			FilenameFilter filter) throws FileNotFoundException, IOException {
		HashMap<String, Instances> map = new HashMap<String, Instances>();

		for (File f : dir.listFiles(filter)) {
			Instances dataset = new Instances(new BufferedReader(
					new FileReader(f)));

			String uid = f.getPath().substring(
					f.getPath().lastIndexOf("_") + 1,
					f.getPath().lastIndexOf("."));

			map.put(uid, dataset);
		}

		return map;
	}

	/**
	 * creates community datasets
	 * 
	 * @param test_dir
	 * @throws Exception
	 */
	private static void createCommunity(File test_dir) throws Exception {
		Map<String, List<String>> map = getUserSongList(new File(
				AI_MapReduce.output_file_path));

		FilenameFilter grade_filter = new FilenameFilter() {
			@Override
			public boolean accept(File arg0, String arg1) {
				return arg1.contains("solograde");
			}
		};
		FilenameFilter comapre_filter = new FilenameFilter() {
			@Override
			public boolean accept(File arg0, String arg1) {
				return arg1.contains("solocompare");
			}
		};

		Map<String, Instances> grade_map = getDatasets(test_dir, grade_filter);
		Map<String, Instances> compare_map = getDatasets(test_dir,
				comapre_filter);

		for (String s : map.keySet()) {
			File grade_path = new File("new_sample/commgrade_" + s + ".arff");
			File compare_path = new File("new_sample/commcomapre_" + s
					+ ".arff");

			Map<String, Double> distances = getDistanceMap(s, map);

			Instances grade_dataset = Weka.communityDataset(
					Weka.userSongGradeAttr(), grade_map, distances);
			Weka.saveARFF(grade_dataset, grade_path);

			Instances compare_dataset = Weka.communityDataset(
					Weka.userSongCompareAttr(), compare_map, distances);
			Weka.saveARFF(compare_dataset, compare_path);
		}

	}

	/**
	 * writes the test output result files
	 * 
	 * @param classfierName
	 * @param solograde_map
	 * @param solocompare_map
	 * @param commgrade_map
	 * @param commcompare_map
	 */
	private static void outputResultFile(String classfierName,
			HashMap<String, Evaluation> solograde_map,
			HashMap<String, Evaluation> solocompare_map,
			HashMap<String, Evaluation> commgrade_map,
			HashMap<String, Evaluation> commcompare_map) {

		try {
			String fileName = "result_" + classfierName + ".csv";

			File f = new File(fileName);

			if (f.exists())
				f.delete();

			FileWriter writer = new FileWriter(fileName);

			writer.append("uid," // user id
					+ "accuracy_solograde," // model accuracy (%)
					+ "accuracy_solocompare,"
					+ "accuracy_commgrade,"
					+ "accuracy_commcompare,"
					+ "precision1_solograde," // precision for class
					+ "precision1_solocompare,"
					+ "precision1_commgrade,"
					+ "precision1_commcompare,"
					+ "precision2_solograde,"
					+ "precision2_solocompare,"
					+ "precision2_commgrade,"
					+ "precision2_commcompare,"
					+ "precision3_solograde,"
					+ "precision3_solocompare,"
					+ "precision3_commgrade,"
					+ "precision3_commcompare,"
					+ "recall1_solograde," // recall for class
					+ "recall1_solocompare,"
					+ "recall1_commgrade,"
					+ "recall1_commcompare,"
					+ "recall2_solograde,"
					+ "recall2_solocompare,"
					+ "recall2_commgrade,"
					+ "recall2_commcompare,"
					+ "recall3_solograde,"
					+ "recall3_solocompare,"
					+ "recall3_commgrade,"
					+ "recall3_commcompare,"
					+ "ROCarea1_solograde," // area under ROC for class
					+ "ROCarea1_solocompare,"
					+ "ROCarea1_commgrade,"
					+ "ROCarea1_commcompare,"
					+ "ROCarea2_solograde,"
					+ "ROCarea2_solocompare,"
					+ "ROCarea2_commgrade,"
					+ "ROCarea2_commcompare,"
					+ "ROCarea3_solograde,"
					+ "ROCarea3_solocompare,"
					+ "ROCarea3_commgrade,"
					+ "ROCarea3_commcompare" + "\n");

			for (String id : solocompare_map.keySet()) {
				String s = id + ",";

				s += solograde_map.get(id).pctCorrect() + ",";
				s += solocompare_map.get(id).pctCorrect() + ",";
				s += commgrade_map.get(id).pctCorrect() + ",";
				s += commcompare_map.get(id).pctCorrect() + ",";

				for (int i = 0; i < 3; ++i) {
					s += solograde_map.get(id).precision(i) + ",";
					s += solocompare_map.get(id).precision(i) + ",";
					s += commgrade_map.get(id).precision(i) + ",";
					s += commcompare_map.get(id).precision(i) + ",";
				}

				for (int i = 0; i < 3; ++i) {
					s += solograde_map.get(id).recall(i) + ",";
					s += solocompare_map.get(id).recall(i) + ",";
					s += commgrade_map.get(id).recall(i) + ",";
					s += commcompare_map.get(id).recall(i) + ",";
				}

				for (int i = 0; i < 3; ++i) {
					s += solograde_map.get(id).areaUnderROC(i) + ",";
					s += solocompare_map.get(id).areaUnderROC(i) + ",";
					s += commgrade_map.get(id).areaUnderROC(i) + ",";
					s += commcompare_map.get(id).areaUnderROC(i) + ",";
				}

				s = s.substring(0, s.length() - 1);

				writer.append(s + "\n");
			}

			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * initiate test
	 * 
	 * @param test_dir
	 * @param classfierName
	 * @throws Exception
	 */
	private static void startTest(File test_dir, String classfierName)
			throws Exception {
		HashMap<String, Evaluation> solograde_map = new HashMap<String, Evaluation>();
		HashMap<String, Evaluation> solocompare_map = new HashMap<String, Evaluation>();
		HashMap<String, Evaluation> commgrade_map = new HashMap<String, Evaluation>();
		HashMap<String, Evaluation> commcompare_map = new HashMap<String, Evaluation>();

		int file_count = 0;

		for (File c : test_dir.listFiles()) {
			if (c.getPath().contains("userSongSummery"))
				continue;

			String uid = c.getPath().substring(
					c.getPath().lastIndexOf("_") + 1,
					c.getPath().lastIndexOf("."));

			Weka w = new Weka(c.getPath());
			Evaluation eval = w.evaluate(classfierName);

			if (c.getPath().contains("solograde"))
				solograde_map.put(uid, eval);
			else if (c.getPath().contains("solocompare"))
				solocompare_map.put(uid, eval);
			else if (c.getPath().contains("commgrade"))
				commgrade_map.put(uid, eval);
			else
				commcompare_map.put(uid, eval);

			// print progress to user:
			file_count++;
			double prg_prcnt = ((double) file_count)
					/ test_dir.listFiles().length;
			prg_prcnt *= 100;
			System.out.println("Evaluation progress: " + (int) prg_prcnt + "%");
		}

		double solograde_sum = 0, solocompare_sum = 0;
		double commgrade_sum = 0, commcompare_sum = 0;
		for (String key : solograde_map.keySet()) {
			solograde_sum += solograde_map.get(key).pctCorrect();
			solocompare_sum += solocompare_map.get(key).pctCorrect();
			commgrade_sum += commgrade_map.get(key).pctCorrect();
			commcompare_sum += commcompare_map.get(key).pctCorrect();
		}

		String classifier = classfierName.substring(classfierName
				.lastIndexOf(".") + 1);

		outputResultFile(classifier, solograde_map, solocompare_map,
				commgrade_map, commcompare_map);

		System.out.println("------------- summery for " + solograde_map.size()
				+ " users using " + classifier + " -------------");
		System.out.println("avg solo grade accuracy: " + solograde_sum
				/ solograde_map.size());
		System.out.println("avg solo compare accuracy: " + solocompare_sum
				/ solocompare_map.size());
		System.out.println("avg community grade accuracy: " + commgrade_sum
				/ commgrade_map.size());
		System.out.println("avg community compare accuracy: " + commcompare_sum
				/ commcompare_map.size());
	}

	public static void main(String[] args) {
		try {
			String input = null;

			if (args.length > 0) // command line parameters
				input = args[0];
			else {
				Scanner scn = new Scanner(System.in);
				do {
					System.out
							.print("use existing sample [Y/N]? "
									+ "otherwise a new random sample will be generated. ");
					input = scn.next();

				} while (!input.equals("Y") && !input.equals("y")
						&& !input.equals("n") && !input.equals("n"));
				scn.close();
			}

			File test_dir = new File("standard_sample");

			if (!input.equals("Y") && !input.equals("y")
					&& !(new File(original_dataset).exists())) {
				System.out
						.println("train_triplets.txt not found. evaluating standard sample");
				input = "Y";
			}

			if (input.equals("N") || input.equals("n")) {
				test_dir = new File("new_sample");
				if (test_dir.exists() && test_dir.isDirectory())
					AI_MapReduce.delete(test_dir);

				test_dir.mkdir();

				DatasetReader rd = new DatasetReader("train_triplets.txt");
				System.out.println("minimize start");
				String file_name = rd.randomMinimizeDS();
				System.out.println("minimize end");

				System.out.println("map-reduce for user files start");
				DatasetReader minimized = new DatasetReader(file_name);
				AI_MapReduce.dataset2userFiles(minimized);
				System.out.println("map-reduce for user files end");

				System.out.println("community files create start");
				createCommunity(test_dir);
				System.out.println("community files create end");

				new File(file_name).delete();
			}

			System.out.println("evaluation start - C4.5");
			startTest(test_dir, "weka.classifiers.trees.J48");
			System.out.println("evaluation start - KNN");
			startTest(test_dir, "weka.classifiers.lazy.IBk");
			System.out.println("evaluation end");

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
