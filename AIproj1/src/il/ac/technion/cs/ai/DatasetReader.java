package il.ac.technion.cs.ai;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Set;

/**
 * @author Michael
 * 
 *         class for gethering statistics of triplet file. also minimizes *
 */
public class DatasetReader {
	private File f;
	private long num_of_rows;

	private HashMap<String, Double> songCountDic;

	public DatasetReader(String path) throws Exception {
		f = new File(path);

		if (!f.exists())
			throw new FileNotFoundException();

		if (!f.isFile())
			throw new IOException("not a file");

		if (!f.canRead())
			throw new IOException("no read permission");

		num_of_rows = -1;
		songCountDic = null;
	}

	/**
	 * @return file row count
	 * @throws Exception
	 */
	public long rowCount() throws Exception {

		if (num_of_rows == -1) {// first count
			BufferedReader br = new BufferedReader(new FileReader(f));

			long mod_operand = 100000;

			try {
				num_of_rows = 0;

				String line;
				line = br.readLine();
				while (line != null) {
					num_of_rows++;
					line = br.readLine();
					if (num_of_rows % mod_operand == 0)
						System.out.println("so far: " + num_of_rows + "rows\n");
				}

			} catch (Exception e) {
				num_of_rows = -1;
				throw e;
			} finally {
				br.close();
			}
		}

		return num_of_rows;
	}

	/**
	 * @return file path
	 */
	public String getFilePath() {
		return f.getPath();
	}

	/**
	 * @return Map of song id to play count
	 * @throws Exception
	 */
	public HashMap<String, Double> userSongcountDic() throws Exception {
		if (songCountDic == null) {
			songCountDic = new HashMap<>();

			BufferedReader br = new BufferedReader(new FileReader(f));
			try {

				String line;
				line = br.readLine();
				while (line != null) {
					String[] params = line.split("	");

					String key = params[0];
					if (songCountDic.containsKey(key)) {
						songCountDic.put(key, songCountDic.get(key) + 1);
					} else {
						songCountDic.put(key, 1.0);
					}

					/*
					 * if(dic.size() % 100 == 0) System.out.println("found " +
					 * dic.size() + "users\n");
					 */

					line = br.readLine();
				}

			} catch (Exception e) {
				throw e;
			} finally {
				br.close();
			}
		}

		return songCountDic;
	}

	/**
	 * @return average listen count
	 * @throws Exception
	 */
	public double listenAvg() throws Exception {
		userSongcountDic();

		long sum = 0;

		for (Double cnt : songCountDic.values()) {
			sum += cnt;
		}

		return (double) sum / songCountDic.size();
	}

	/**
	 * @return maximum listen count
	 * @throws Exception
	 */
	public Double listenMax() throws Exception {
		userSongcountDic();

		Double max = -1.0;

		for (Double cnt : songCountDic.values()) {
			if (max < cnt)
				max = cnt;
		}

		return max;
	}

	/**
	 * @return minimum listen count
	 * @throws Exception
	 */
	public Double listenMin() throws Exception {
		userSongcountDic();
		Double min = Double.MAX_VALUE;

		for (Double cnt : songCountDic.values()) {
			if (min > cnt)
				min = cnt;
		}

		return min;
	}

	private final double minimizing_ratio = 0.0002;

	/**
	 * minimizes the triplet file to a small random subset. size is determined
	 * by minimizing_ratio.
	 * 
	 * @return result file path
	 * @throws Exception
	 */
	public String randomMinimizeDS() throws Exception {
		List<String> list = new LinkedList<String>(userSongcountDic().keySet());
		Collections.shuffle(list);
		Set<String> randomSet = new HashSet<String>(list.subList(0,
				(int) (list.size() * minimizing_ratio)));

		Random rnd = new Random();

		String file_name = "random_subset_" + rnd.nextInt(1000) + ".tsv";

		File subset = new File(file_name);
		subset.createNewFile();
		subset.setWritable(true);

		Writer writer = null;

		BufferedReader br = new BufferedReader(new FileReader(f));
		writer = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(subset), "utf-8"));

		int cnt = 0;
		try {
			String line;
			line = br.readLine();
			while (line != null) {
				String key = line.split("	")[0];

				if (randomSet.contains(key)) {
					writer.write(line + "\n");
					cnt++;
				}

				line = br.readLine();
			}

			System.out.println("reduced to " + randomSet.size()
					+ " users with avg of " + +(double) cnt / randomSet.size()
					+ " listens\n");

		} catch (Exception e) {
			throw e;
		} finally {
			br.close();
			writer.close();
		}

		return file_name;
	}
}
