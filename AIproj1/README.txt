# Song preference prediction system #

### under "Project in AI 236502" course

This java project creates and analyzes datasets containing user listen history and 
music preferences, aiming to find best methods and algorithms to predict for new 
songs and tracks.

### HOW TO RUN
To run use <java -jar ai_run.jar> from main project directory and follow on screen 
instructions, or add command line parameters:
y - run test on standard sample
n - build and run tests on new random sample

Build (compile) project using eclipse only!

* Generating new random test sample is possible only if train_triplets.txt is present in 
main project directory.

### IMPORTANT
Before you run this code please make sure:
* You're running on LINUX based OS. If you're running on WINDOWS please
download CYGWIN (https://www.cygwin.com/) and add bins to local variables.
* Download and install WEKA (http://www.cs.waikato.ac.nz/ml/weka/)for *.arff GUI.
* For running test on new random user subset download train_triplets.txt dataset 
from (http://labrosa.ee.columbia.edu/millionsong/tasteprofile) under "Getting the dataset"
fully unpack and copy to project main folder.

### Files
* AI_MapReduce.java - methods for running map-reduce and create user datasets
* DatasetReader.java - class and methods to gather statistics and minimize large sample
* MainClass.java - main test method
* SongAPI.java - methods using EchoNest API to gather metadata from song ids
* Weka.java - machine learning methods and cross-validation algorithms.

### For more information please contact 
### Michael Varvaruk
### mvar@t2.technion.ac.il


