# README #

Java project 'Project in AI" Techinon course.

by Michael Varvaruk 308571256

### What is this repository for? ###

* AI system for creating and analyzing user music preferences dataset
* V 1.0
* For more information contact mvar@t2.technion.ac.il

### How do I get set up? ###

* Download code
* If you're running on windows please install Cygwin from https://www.cygwin.com/ and make sure linux       commands are accessble through cmd.
* Once you run application follow on screen instructions.